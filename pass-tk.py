#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, time, random
from threading import Thread

win32 = sys.platform=="win32"

import tkinter as tk
from tkinter import ttk	#, messagebox as mb

# logd с указанием файла и номера строки
from inspect import currentframe, getframeinfo
__logd_spacer = "\t"*10
def get_linenumber():
	cf = currentframe()
	return cf.f_back.f_back.f_lineno
def get_filename():
	cf = currentframe()
	return getframeinfo(cf.f_back).filename
def logd(*args, **keywords):
	msg = "\t".join(map(str,args))
	print("%s%sFile \"%s\", line %s"%(
		msg, __logd_spacer, get_filename(), get_linenumber()))

from translate import tr

from helpertk import *

DEBUG		= True
DEBUG		= False

MYFILEPATH	= os.path.realpath(os.path.abspath(__file__))
MYPATH		= os.path.dirname(MYFILEPATH)
HOMEPATH	= os.environ["USERPROFILE" if win32 else "HOME"]
DATAPATH	= os.path.join(HOMEPATH, ".password-store")
CHECKFILE	= os.path.join(DATAPATH, "checkfile.gpg")
SHMTMP		= "/dev/shm/ssaptsal"
IMAGESPATH	= os.path.join(MYPATH, "images")
SOUNDSPATH	= os.path.join(MYPATH, "sounds")
LAUGHSPATH	= os.path.join(SOUNDSPATH, "laugh")
CLIPBOARDED = os.path.join(SOUNDSPATH, "prota_kerching.ogg")
ICON_FILE_NAME		= os.path.join(IMAGESPATH, "pass-qt.png")
FOLDER_IMAGE_NAME	= os.path.join(IMAGESPATH, "folder-16x16.png")
FILE_IMAGE_NAME		= os.path.join(IMAGESPATH, "document-locked-16x16.png")
APP_TITLE			= tr("Logins and passwords")
IGNORE_ITEMS=(
	"push-to-repo",
	".hg",
	".git",
	".gpg-id",
	"pull-from-repo",
	"work-with-repo.sh",
	"checkfile.gpg",
)
SPECIAL_ITEMS={
	"яz.thrash":("Корзина", os.path.join(IMAGESPATH, "trash-full-16x16.png")),
}
ASPECIAL_ITEMS={
	"Корзина":"яz.thrash",
}
SOUNDS	= list(
	map(lambda x: os.path.join(LAUGHSPATH, x), os.listdir(LAUGHSPATH)))

#~ logd	= print
do_func	= os.system

def async(func):
	def wrapper(*args, **kwargs):
		thr = Thread(target = func, args = args, kwargs = kwargs)
		thr.start()
	return wrapper

def play_ogg(fn):
	if os.path.exists(fn):
		do_func("ogg123 "+fn+" >/dev/null 2>&1 &")
	#~ else:
		#~ do_func("DISPLAY=" + DISPLAY + " NARRATOR=" + random.choice(NARRATORS) + \
			#~ " say \"Звук не найден.\"")

def clear_treeview(tv):
	for item in tv.get_children():
			tv.delete(item)

class Application(tk.Frame):
	def __init__(self, master=None, title="", iconfilename=""):
		super().__init__(master)
		self.folder_image = tk.PhotoImage(file=FOLDER_IMAGE_NAME)
		self.file_image = tk.PhotoImage(file=FILE_IMAGE_NAME)
		self.master.wm_title(title)
		self.master.iconphoto(True, tk.PhotoImage(file=iconfilename))

		self.show_pass = True
		self.minimize_after_show_pass = True

		# enter instead space. no animation
		#~ self.master.bind_class("Button", "<Key-Return>",
			#~ lambda event: event.widget.invoke()
		#~ )

		#~ self.master.configure(background="red")
		#~ self.configure(background="green")
		self.pack(fill="both", expand=True)
		self.rowconfigure(0, weight=1)
		self.columnconfigure(0, weight=1)
		self.rowconfigure(1, weight=0)
		self.columnconfigure(1, weight=0)
		self.create_widgets()
		self.center()
		self.master.update_idletasks()

	def center(self):
		if os.environ["COMPUTERNAME" if win32 else "HOSTNAME"] in ("slairium.net", \
			"a460383.izh-auto.local.net"):
			center(self.master, True)
		else:
			center(self.master)

	def create_widgets(self):
		s = ttk.Style()
		s.theme_use("clam")
		#~ logd("TODO:", "подогнать", "стиль", "ttk")
		s.configure("Treeview", background="#deebc5", foreground="#000000")
		s.configure("Treeview.Item", focuscolor="#4a6984")

		self.f_tree = make_frame(self, 0, 0, 1, 1, "nswe", padx=2, pady=2)
		self.treeScroll = make_ttk_scrollbar(self.f_tree, 1, 0, 1, 1, "ns")
		#~ self.tv_passwords = make_ttk_searchable_treeview(self.f_tree,
		self.tv_passwords = make_ttk_treeview(self.f_tree,
			0, 0, 1, 1, "nswe",
			selectmode="browse", show="tree",
			height=20, yscrollcommand = self.treeScroll.set,

		)
		self.tv_passwords.extra={"name":"tv_passwords"}
		self.tv_passwords.bind("<KeyPress>", self.on_keypressed)
		self.tv_passwords.bind("<Double-Button-1>", self.on_dbl_click)
		self.tv_passwords.bind("<Prior>", self.prior_pressed)
		self.tv_passwords.bind("<Next>", self.prior_pressed)
		self.tv_passwords.column("#0", minwidth=16, width=480)
		self.treeScroll.config(command=self.tv_passwords.yview)
		self.f_tree.columnconfigure(0, weight=1)
		self.f_tree.columnconfigure(1, weight=0)
		self.f_tree.rowconfigure(0, weight=1)
		self.f_tree.rowconfigure(1, weight=0)
		self.f_tree.grid_remove()

		self.f_mainpwd = make_frame(self, 0, 0, 1, 1, "nswe",
			pady=2
		)
		self.l_pwd = make_label(self.f_mainpwd, 0, 0, 1, 1, "e",
			text=tr("Main password:")
		)
		self.l_pwd.grid(padx=2)
		self.le_pwd = make_entry(self.f_mainpwd, 1, 0, 1, 1, "we", show="*")
		self.le_pwd.extra={"name":"le_pwd"}
		self.le_pwd.bind("<KeyPress>", self.on_keypressed)
		self.b_checkpwd = make_button(self.f_mainpwd, 2, 0, 1, 1, "w",
			text=tr("Check"), command=self.check_mainpwd, padx=2
		)
		self.b_checkpwd.grid(padx=2)
		self.f_mainpwd.columnconfigure(0, weight=1)
		self.f_mainpwd.columnconfigure(1, weight=1)
		self.f_mainpwd.columnconfigure(2, weight=1)
		self.f_mainpwd.columnconfigure(3, weight=0)
		self.f_mainpwd.rowconfigure(0, weight=1)
		self.f_mainpwd.rowconfigure(1, weight=0)

		self.le_pwd.focus_set()

	def prior_pressed(self, e):
		#~ self.master.update()
		#~ self.tv_passwords.update()
		#~ logd(e.x, e.y)
		#~ iid = e.widget.identify_element(40, 11)
		#~ e.event_generate("<>")
		e.widget.after_idle(self.set_selection, e)
		"""
		iid = e.widget.identify_row(10)
		logd("'%s'"%iid)
		if iid:
			e.widget.focus(iid)
			e.widget.selection_set(iid)
		"""

	def set_selection(self, e):
		iid = e.widget.identify_row(10)
		#~ logd("'%s'"%iid)
		if iid:
			e.widget.focus(iid)
			e.widget.selection_set(iid)

	def quit(self):
		self.master.destroy()
		sys.exit(0)

	def on_dbl_click(self, e):
		iid = e.widget.selection()[0]
		values = e.widget.item(iid, "values")
		if values:
			if e.state&4:
				self.show_pass = False
			if e.state&1:
				self.minimize_after_show_pass = False
			self.show_pwd(values[0])

	def on_keypressed(self, e):
		if not e.keysym:
			return
		#~ logd("%s"%bin(e.state))
		ks = e.keysym
		if ks=="Escape":
			self.quit()
		elif ks=="KP_Enter" or ks=="Return":
			if e.widget.extra["name"]=="le_pwd":
				anim_b_press(self.b_checkpwd).invoke()
			else:
				iid = e.widget.selection()
				if e.widget.item(iid)["values"]:
					if e.state&4:
						self.show_pass = False
					if e.state&1:
						self.minimize_after_show_pass = False
					self.show_pwd(e.widget.item(iid)["values"][0])
				else:
					if ks=="KP_Enter":
						e.widget.item(iid, open=not e.widget.item(iid, "open"))
		elif ks=="F5":
			if e.widget.extra["name"]=="tv_passwords":
				clear_treeview(e.widget)
				self.fill_tv(DATAPATH, e.widget)
				e.widget.focus_set()
				e.widget.focus(e.widget.get_children()[0])
				e.widget.selection_set(e.widget.get_children()[0])
		#~ else:
			#~ logd(ks)

	def show_pwd(self, pwd_fn):
		s = self.decode_file(pwd_fn)

		self.master.clipboard_clear()
		self.master.clipboard_append(s)
		play_ogg(CLIPBOARDED)

		if self.show_pass:
			mb = MessageBox(pwd_fn, s, master=tk.Tk())
		self.show_pass = True

		if self.minimize_after_show_pass:
			self.master.iconify()

	def check_mainpwd(self, *args, **keywords):
		self.__pwd = self.le_pwd.get()
		if self.pwd_is_valid():
			# TODO: animate window
			#~ self.master.geometry("800x488+0+0")
			#~ endposx, endposy = self.get_center_window()
			# place to below screen
			#~ self.master.geometry("800x488+%s+%s"%(endposx, 0))
			clear_treeview(self.tv_passwords)
			self.fill_tv(DATAPATH, self.tv_passwords)
			self.f_mainpwd.grid_remove()
			self.f_tree.grid(row=0, column=0, sticky="ewns")
			self.master.geometry("800x488")
			self.center()
			self.tv_passwords.focus_set()
			self.tv_passwords.focus(self.tv_passwords.get_children()[0])
			self.tv_passwords.selection_set(self.tv_passwords.get_children()[0])
		else:
			play_ogg(random.choice(SOUNDS))
			self.le_pwd.delete(0, "end")
			self.le_pwd.focus_set()
			self.shake_window()

	def shake_window(self):
		g = self.master.geometry()
		size = g[:g.find("+")]
		sizex,sizey = map(int, size.split("x"))
		pos = g[g.find("+")+1:]
		posx,posy = map(int, pos.split("+"))
		start_time = time.time()
		self.master.geometry("+%s+%s"%(posx, posy+3))
		while time.time()-start_time<0.6:
			rx = random.randint(1,16)
			ry = random.randint(1,16)
			self.master.geometry("+%s+%s"%(posx-16+rx, posy-16+ry))
			self.master.update()
			time.sleep(0.01)
		self.master.geometry("+%s+%s"%(posx, posy))

	def decode_file(self, fn):
		if DEBUG: return "debug is active"
		if os.path.exists(SHMTMP):
			os.unlink(SHMTMP)

		oscmd = "gpg --no-tty --batch --passphrase " + \
			"'%s' --output '%s' --decrypt '%s' > /dev/null 2>&1"%(self.__pwd,
			SHMTMP, fn)

		os.system(oscmd)
		if os.path.exists(SHMTMP):
			f = open(SHMTMP, "r")
			data = f.read().strip()
			f.close()
			os.unlink(SHMTMP)
			return data
		else:
			return None

	def pwd_is_valid(self):
		if DEBUG: return True
		if self.decode_file(CHECKFILE):
			return True
		return False

	def fill_tv(self, folder_path, tv, parent=None):
		data=os.listdir(folder_path)
		data.sort(key=str.lower)
		data.reverse()
		for item in data:
			if not item in IGNORE_ITEMS:
				fullpath_item = os.path.join(folder_path, item)
				if os.path.isdir(fullpath_item):

					new_text = item
					new_image = self.folder_image

					if item in SPECIAL_ITEMS:
						new_text = SPECIAL_ITEMS[item][0]
						logd(SPECIAL_ITEMS[item][1])
						new_image = tk.PhotoImage(file=SPECIAL_ITEMS[item][1])
						logd(new_image)

					if parent:
						# folder at parent
						new_folder_item = tv.insert(parent, 0,
							image=self.folder_image, text=item)
					else:
						# folder at top
						new_folder_item = tv.insert("", 0,
							image=new_image, text=new_text)
						logd(new_image)

					self.fill_tv(fullpath_item, tv, new_folder_item)
				else:
					if item[-4:]!=".gpg":
						continue
					item = item[:-4]
					if parent:
						# file at parent
						new_file_item=tv.insert(parent, 0,
							image=self.file_image, text=item,
							values=(fullpath_item,)
						)
					else:
						# file at root
						new_file_item = tv.insert("", "end",
							image=self.file_image, text=item,
							values=(fullpath_item,)
						)

class MessageBox(tk.Frame):
	def __init__(self, header="header", msg="message", timeout=0,
		iconfilename=None, master=None,
	):
		super().__init__(master)
		# enter instead space. no animation
		self.master.bind_class("Button", "<Key-Return>",
			lambda event: event.widget.invoke()
		)
		self.header = header
		self.msg = msg
		if "\n" in msg:
			self.lines = len(msg.split("\n"))+2
		else:
			self.lines = 3
		self.timeout = timeout
		#~ self.info_image = tk.PhotoImage(file="lock-open.png")
		self.master.wm_title(header)
		#~ if iconfilename:
			#~ self.master.iconphoto(True, tk.PhotoImage(file=iconfilename))
		#~ else:
			#~ self.master.iconphoto(True, tk.PhotoImage(file="lock-open.png"))
		self.pack(fill="both", expand=True)
		self.rowconfigure(0, weight=1)
		self.columnconfigure(0, weight=1)
		self.rowconfigure(1, weight=0)
		self.columnconfigure(1, weight=0)
		self.create_widgets()
		self.b_ok.focus_set()
		#~ self.result = None
		if os.environ["COMPUTERNAME" if win32 else "HOSTNAME"] in ("slairium.net", \
			"a460383.izh-auto.local.net"):
			center(self.master, True)
		else:
			center(self.master)
		self.lift()

	def copytext(self):
		"""Копирование выделенных данных в буфер обмена."""
		try:
			text_to_clipboard = self.t_message.get("sel.first", "sel.last")
		except tk.TclError:
			#~ print "Выделенной области нет"
			text_to_clipboard = ""
		self.master.clipboard_clear()
		self.master.clipboard_append(text_to_clipboard)

	def call_cmenu(self, event):
		"""Отображение контекстного меню."""
		#~ self.t_message_focused()
		pos_x = self.t_message.winfo_rootx() + event.x
		pos_y = self.t_message.winfo_rooty() + event.y

		try:
			text_to_clipboard = self.t_message.get("sel.first", "sel.last")
		except tk.TclError:
			text_to_clipboard = ""
		if text_to_clipboard:
			self.cmenu.entryconfig("Копировать", state="normal")
		else:
			self.cmenu.entryconfig("Копировать", state="disabled")

		#~ try:
			#~ text_to_clipboard = self.master.clipboard_get()
		#~ except tk.TclError:
			#~ text_to_clipboard = ""
		#~ if text_to_clipboard:
			#~ self.cmenu.entryconfig("Вставить", state="normal")
		#~ else:
			#~ self.cmenu.entryconfig("Вставить", state="disabled")


		self.cmenu.tk_popup(pos_x, pos_y)

	def create_widgets(self):

		self.t_message = make_text(self, 0, 0, 2, 1,
			height=self.lines, highlightbackground=COL_BUTTONFACE, \
			relief="sunken", bg=COL_WINDOWBG, padx=10, bd=1, \
			font=("","10"), highlightthickness=10, width=60
		)
		self.t_message.insert(1.0, "\n"+self.msg+"\n")
		self.t_message.tag_add("all", "1.0", tk.END)
		self.t_message.config(state="disabled")
		self.t_message.tag_config("all", justify="center")
		self.cmenu = tk.Menu(self.t_message, tearoff=False)
		self.cmenu.add_command(label="Копировать", command=self.copytext)
		#~ self.cmenu.add_command(label="Вставить", command=self.pastetext)
		self.t_message.bind("<ButtonRelease-3>", self.call_cmenu)


		self.b_cancel = make_button(self, 0, 1, 1, 1, "w", \
			text="Cancel", \
			command=self.b_cancel_pressed
		)
		self.b_cancel.grid(padx=10, pady=4)
		self.b_cancel.bind("<Escape>", \
			self.b_cancel_pressed)


		self.b_ok = make_button(self, 1, 1, 1, 1, "e", \
			text="Ok", \
			command=self.b_ok_pressed
		)
		self.b_ok.grid(padx=10, pady=4)
		self.b_ok.bind("<Escape>", \
			self.b_cancel_pressed)

	def b_cancel_pressed(self, event=None):
		#~ self.result = self.defer_min
		self.master.destroy()
		#~ cmd = 'echo ' + "DISPLAY=" + DISPLAY + ' atalarmer.py -s "' + \
			#~ self.msg.replace(" ", "\\ ") + '" | at now +' + \
			#~ str(self.defer_min) + ' min' + HIDE_OUTPUT
		#~ do_func(cmd)

	def b_ok_pressed(self, event=None):
		self.master.destroy()

def main():
	app = Application(master=tk.Tk(), title=APP_TITLE,
		iconfilename=ICON_FILE_NAME)
	#~ app = MessageBox("заголовок", "сообщение")
	app.mainloop()

if __name__=='__main__':
	main()
