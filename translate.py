#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import locale

tr_dict = {
"ru_RU": {
	"Logins and passwords":"Логины и пароли",
	"Main password:":"Основной пароль",
	"Check":"Проверить",
	}
}

lc = locale.getdefaultlocale()[0]

logw = print

def tr(msg):
	if msg in tr_dict[lc]:
		return tr_dict[lc][msg]
	else:
		logw("\t\"%s\":\"\","%msg)
		return msg

def main():
	#~ print(len(sys.argv))
	#~ if len(sys.argv)>1:
		#~ a = sys.argv[1]
	pass

if __name__=='__main__':
	main()
